angular.module('starter.services', [])

.factory('VeillesFactory', function($http){
  return {
    getAll:function() {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=getAll');
    },
    get:function(veilleId) {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=get&id='+veilleId);
    }
  }
})

.factory('UsersFactory', function($http){
  return {
    getAll:function() {
      return $http.get('http://veille.popschool.fr/api/?api=user&action=getAll');
    },
    get:function(userId) {
      return $http.get('http://veille.popschool.fr/api/?api=user&action=get&id='+userId);
    },
    getByUser:function (userId) {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=getByUser&id_user=' + userId);
    }
  }
})