angular.module('starter.controllers', [])

.controller('UsersCtrl', function($scope, UsersFactory){
  $scope.users = [];
  UsersFactory.getAll().then(function(r){
    $scope.users = r.data.users;
  });
})

.controller('UserDetailCtrl', function($scope, $stateParams, UsersFactory) {
  UsersFactory.getByUser($stateParams.userId).then(function(r){
    $scope.veilles = r.data.veilles;
    //console.log($scope.veilles);
  });
  UsersFactory.get($stateParams.userId).then(function(r){
    //console.log(r.data.user[0]);
    $scope.user = r.data.user[0];
    //console.log($scope.veille);
  });
  //console.log($stateParams.veilleId)
})

.controller('VeillesCtrl', function($scope, VeillesFactory){
  $scope.veilles = [];
  VeillesFactory.getAll().then(function(r){
    $scope.veilles = r.data.veilles;
  });
})

.controller('VeilleDetailCtrl', function($scope, $stateParams, VeillesFactory) {
  VeillesFactory.get($stateParams.veilleId).then(function(r){
    $scope.veille = r.data.veille;
    //console.log($scope.veille);
  });
  //console.log($stateParams.veilleId)
})
